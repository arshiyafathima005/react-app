import React, { Component } from 'react';
import { StyleSheet, View,ScrollView} from 'react-native';
import Forms from '../components/paperComponents/Forms';
import {Button} from 'react-native-paper';
import {HideWithKeyboard} from 'react-native-hide-with-keyboard';
import AppBar from './paperComponents/Appbars'

/*export default class FirstPage extends Component {
    static navigationOptions = {
      title: 'First Page',
      //Sets Header text of Status Bar
      headerStyle: {
        backgroundColor: '#f4511e',
        //Sets Header color
      },
      headerTintColor: '#fff',
      //Sets Header text color
      headerTitleStyle: {
        fontWeight: 'bold',
        //Sets Header text style
      },
    };
  
    render() {
      const { navigate } = this.props.navigation;
      return (
        <View style={styles.container}>
          <Button title='Go next'
          onPress={() =>navigate('SecondPage')}
          />
        </View>
      );
    }
  }
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });*/
  const FirstScreen=({navigation})=> {
    return (
      <ScrollView stickyHeaderIndices={[0]}>
        {/*  <AppBar/>  */}
      <Forms/>
     <HideWithKeyboard>
        <Button onPress={() => navigation.navigate('Details')}>Details</Button>
      </HideWithKeyboard>
      </ScrollView>
    );
  }
  export default FirstScreen;