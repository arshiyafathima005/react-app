import React, {useReducer, useCallback} from 'react';
import {View, StyleSheet} from 'react-native';
import {TextInput, Text, Button, Divider} from 'react-native-paper';

const initialState = {
  number1: 0,
  number2: 0,
  result: 0,
};
function reducer(state = initialState, action) {
  switch (action.type) {
    case 'UPDATE_NUMBER1':
      return {
        ...state,
        number1: action.payload,
      };
    case 'UPDATE_NUMBER2':
      return {
        ...state,
        number2: action.payload,
      };
    case 'RESULT':
      return {
        ...state,
        result: (+state.number1) + (+state.number2),
        //+ is used to convert string to number (added in react JS 0.60 version ).we can also use (parseInt) for conversion
        //if we not convert it will consider these two numbers as string and concats the numbers passed in input fields
      };
    default:
      return state;
  }
}

//
const CallBack = () => {
  const [state, setNum] = useReducer(reducer, initialState);

  //we should store callBack funtion in a variable
  //useCallback is a memoize function that memoize the function and store function result in the cache. 
  //if the funtion is called for the same value again it will not trigger the 
  //funtion for execution it just displays the cached value.
  const resultMomized = useCallback(() => {
    setNum({type: 'RESULT'});
    console.warn(`${state.result}`);
  }, [state.result]);
  //in this array we should pass a dependencies which we want for callBacks
  return (
    <View>
      <Text style={styles.text}>Enter the Numbers You Want's to Calculate..!</Text>
      <TextInput
        label="Enter Number 1"
        keyboardType="numeric"
        mode="outlined"
        value={state.number1}
        onChangeText={(number1) =>
          setNum({type: 'UPDATE_NUMBER1', payload: number1})
         
        }
        style={styles.textInptStyles}
        right={<TextInput.Icon color="#8C8C8C" icon="calculator" size={30} />}
      />
      <TextInput
        label="Enter num 2"
        keyboardType="numeric"
        mode="outlined"
        value={state.number2}
        onChangeText={number2 =>
          setNum({type: 'UPDATE_NUMBER2', payload: number2})
        }
        style={styles.textInptStyles}
        right={<TextInput.Icon color="#8C8C8C" icon="calculator" size={30} />}
      />
     <Button
        style={{margin: 20}}
        icon="loading"
        mode="contained"
        onPress={() => {
          resultMomized();
        }}
        color="#1a61d8"
        contentStyle={{height: 40}}>
        Calculate
      </Button>
      <Text style={styles.text}>Result:{state.result}</Text>
    </View>
  );
};
export default CallBack;

const styles = StyleSheet.create({
  textInptStyles: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
    fontSize: 16,
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 20,
  }
});
