import React, {useReducer} from 'react';
import {} from 'react-native';
import {} from 'react-native-paper';
const initialState = {
  additionalPrice: 0,
  car: {
    price: 26395,
    name: '2019 ford mustang',
    image: '',
    features: [],
  },
  store: [
    {id: 1, name: 'V-6 engine', price: 1500},
    {id: 2, name: 'Racing detail package', price: 1500},
    {id: 3, name: 'Premium sound system', price: 500},
    {id: 4, name: 'Rear spoiler', price: 250},
  ],
};
const reducer = (state, action) => {
  switch (action.type) {
    case 'REMOVE_ITEM':
      return {
        ...state,
        additionalPrice: state.additionalPrice - action.item.price,
        car: {
          ...state.car,
          features: state.car.features.filter(x => x.id !== action.item.id),
        },
        store: [...state.store, action.item],
      };
    case 'BUY_ITEM':
      return {
        ...state,
        additionalPrice: state.additionalPrice + action.item.price,
        car: {...state.car, features: [...state.car.features, action.item]},
        store: state.store.filter(x => x.id !== action.item.id),
      };
    default:
      return state;
  }
};
const CarUseReducer = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const removeFeature = (item) => {
    dispatch({ type: 'REMOVE_ITEM', item });
  }
  
  const buyItem = (item) => {
    dispatch({ type: 'BUY_ITEM', item })
  }
  return(
    <View>
        <Image source={{uri: 'https://url_of_image.png'}}  
       style={{width: 60, height: 60}} />  
    </View>
  ) ;
};
export default CarUseReducer;
