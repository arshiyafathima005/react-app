import React, {useState, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import {Button, TextInput, Text} from 'react-native-paper';
const UseEffectHook = () => {
  const [count, setCount] = useState(0);
  const [count2, setCount2] = useState(0);
  useEffect(() => {
    alert("I am clicked");
    console.log('use effect')
  },[count]);
  return (
    <View>
      <Button onPress={() => setCount(count + 1)}>
        You pressed {count} times
      </Button>
      <Button onPress={() => setCount2(count2 + 1)}>
        You pressed {count2} times
      </Button>
    </View>
  );
};
export default UseEffectHook;
const styles = StyleSheet.create({
  textInptStyles: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,

    fontSize: 16,
  },
});
