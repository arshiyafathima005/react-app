import React, {useState, useEffect} from 'react';
import {View, Text, Button} from 'react-native';
import PropsUseEffects from './PropsUseEffects';
//use effect with props


//using states
const UseEffects=()=>{
const [count,setCount]=useState(0);
//const [data,setData]=useState('No data')
const [data,setData]=useState(10)
useEffect(()=>{
  if(count==5){
    setData('NEW DATA UPDATED')
  }
  else if(count==10){
      setData('Updated Data to 10')
  }
})
return(
   
    <View style={ {flex:1,marginTop:100}}>
       {/*  <Text style={{fontWeight:'bold',fontSize:20,marginLeft:180}}>{count}</Text>
        <Text style={{fontWeight:'bold',fontSize:20,marginLeft:180}}>{data}</Text>
     */} 
     <PropsUseEffects data={data}/>
    {/*  <Button title="Count" onPress={()=>{setCount(count+1)}}/>
  */} 
   <Button title="Count" onPress={()=>{setData(data+10)}}/>
 
    </View>
)
}

export default UseEffects;
