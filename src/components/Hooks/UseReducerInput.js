﻿import React, {useReducer, useEffect, useCallback} from 'react';
import {View, StyleSheet, ToastAndroid} from 'react-native';
import {TextInput, Text, Button, Divider} from 'react-native-paper';

//initializing variables for reducers
const initialState = {
  name: '',
  phone: 0,
  age: 0,
  error: '',
  count: 0,
};

//reducer is a function which takes two arguments---> current state and an action
//it returns a new state based on both arguments
//The action payload provides information for the state change.
function reducer(state = initialState, action) {
  switch (action.type) {
    case 'UPDATE_NAME':
      return {
        ...state,
        name: action.payload,
      };

    case 'UPDATE_PHONE':
      return {
        ...state,
        phone: action.payload,
      };
    case 'UPDATE_AGE':
      return {
        ...state,
        age: action.payload,
      };
    /*case 'ERROR':
      return {
        ...state,
        error: 'Some Thing Went Wrong',
      };*/
    case 'COUNT':
      return {
        ...state,
        count: state.count + 1,
      };
    default:
      return state;
  }
}

const UseReducerInput = () => {
  //we are defining a use reducer funtion here
  const [state, dispatch] = useReducer(reducer, initialState);
  const handleChangeText = name =>
    dispatch({
      type: 'UPDATE_NAME',
      payload: name,
    });

  //use effect execute each time after every render call.
  //We should use useEffect hook inside a component
  useEffect(() => {
    //when state is changed each time useeffect will execute
    alert(
      `Submition Successful!! \n User Name is ${state.name} \n Phone Number:${state.phone} \n Application No:${state.count}`,
    );
    //React performs the cleanup when the component unmounts
    return () => {
      ToastAndroid.show('CLEAN UP!', ToastAndroid.SHORT);
    };
  }, [state.count]);
  //here we can only re-run the effect if count changes

  //useCallback is a memoize function that store the value in the cache. 
  //if the funtion with same value is called again it will not trigger the 
  //funtion for execution it just displays the cached value
  const memoizedCallBack=useCallback(()=>{
    console.warn(`${state.name}`)
    dispatch({type:'UPDATE_NAME'})
   // alert(`${state.name}`)
   },[state.name])

  return (
    <View>
      {console.log(state)}
      <Text style={styles.text}>Enter User Information</Text>
      <TextInput
        label="Name"
        mode="outlined"
        value={state.name}
        onChangeText={name => handleChangeText(name)}
        style={styles.textInptStyles}
        autoCapitalize="words"
        autoCompleteType="name"
      />
      <TextInput
        label="Phone Number"
        keyboardType="numeric"
        mode="outlined"
        maxLength={10}
        value={state.phone}
        onChangeText={e => dispatch({type: 'UPDATE_PHONE', payload: e})}
        style={styles.textInptStyles}
        left={<TextInput.Affix color text="+91" size={30} />}
        right={<TextInput.Icon color="#8C8C8C" icon="phone" size={30} />}
      />
      <TextInput
        label="Age"
        keyboardType="numeric"
        mode="outlined"
        maxLength={2}
        value={state.age}
        onChangeText={age => dispatch({type: 'UPDATE_AGE', payload: age})}
        style={styles.textInptStyles}
        right={<TextInput.Icon color="#8C8C8C" icon="age" size={30} />}
      />
      <Button
        style={{margin: 20}}
        icon="loading"
        mode="contained"
        onPress={() => {
          dispatch({type: 'COUNT'});
        }}
        color="#1a61d8"
        contentStyle={{height: 40}}>
        Submit
      </Button>
      <Button
        style={{margin: 20}}
        icon="loading"
        mode="contained"
        onPress={() => {memoizedCallBack()}}
        color="#1a61d8"
        contentStyle={{height: 40}}>
        Display List
      </Button>
      <Divider />

      <Text style={styles.text}>Display Information</Text>
      <Text style={styles.disText}>Name:{state.name}</Text>
      <Text style={styles.disText}>Phone Number:{state.phone}</Text>
      <Text style={styles.disText}>Applications No:{state.count}</Text>
      {/*  <Text style={styles.disText}>{state.error ? state.error : null}</Text>*/}
      {}
      <Divider />
    </View>
  );
};
export default UseReducerInput;
const styles = StyleSheet.create({
  textInptStyles: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
    fontSize: 16,
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  disText: {
    marginLeft: 10,
  },
});
