import React,{useReducer,useState,useCallback} from 'react';
import {View, StyleSheet} from 'react-native';
import {TextInput, Text, Button,Divider} from 'react-native-paper';

const initialState={
    comment: [],
    number:0
}
/*const action={
    type:'user_info',
    name:'Arshiya',
    phone:'7930990001'
}*/
const reducer=(state,action)=>
{
    switch(action.type)
    {
        case 'comment':
         return  {
            comment: [
              ...state.comment,
              {text:action.text,completed:false}
            ],
            number:state.number+1
          };
          /*case 'add-no':{
            todos:[
              ...state.todos,
              {
                number:action.number,completed:false
              }
            ]
          };*/
        
         default:return state;
        
}
}

const UseReducerPractice = () => {
    const [{comment,number},dispatch]=useReducer(reducer,initialState)
    const [text,setText]=useState("");
    //const [number,setNumber]=useState("");

    const todo=useCallback(()=>{
      console.log(`${text}`)
      dispatch({type:"comment",text})
     
    },[comment])
    return (
    <View>
      <Text style={styles.text}>Enter Comments</Text>
      <TextInput
        label="Comment Box..."
        value={text}
        mode="outlined"
        multiline={true}
        numberOfLines={7}
        onChangeText={text=>setText(text)}
        style={styles.textInptStyles}
        autoCapitalize="words"
        autoCompleteType="name"
      />
     {/*  <TextInput
        label="Phone Number"
        keyboardType="numeric"
        mode="outlined"
        maxLength={10}
        //value={number}
        //onChangeText={number=>setNumber(number)}
        style={styles.textInptStyles}
        left={<TextInput.Affix color text="+91" size={30} />}
        right={<TextInput.Icon color="#8C8C8C" icon="phone" size={30} />} 
      />*/}
      <Button
        style={{margin: 20}}
        icon="loading"
        mode="contained"
        onPress={() => {todo()}}
        color="#1a61d8"
       
        contentStyle={{height: 40}}>
        Submit
      </Button>
      <Divider/>
      
    <Text style={styles.text}>Display Comments</Text>
     {/*  <Text>{JSON.stringify(todos,null,2)}</Text>
      <Text></Text> */}
       <Text style={styles.disText}>Number of Comments:{number}</Text>
       <Divider/>
    {comment.map(t=>(<Text key={t.text} style={styles.disText}>Comments:{t.text}</Text>))}
      
    </View>
  );
};
export default UseReducerPractice;
const styles = StyleSheet.create({
  textInptStyles: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
    fontSize: 16,
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  disText:{
    marginLeft:10
  }
});


/*import React from 'react'; 
const initialTodo = [
   { id: 'a',
    task: 'Do Something',
     complete: false,   }, 
   { id: 'b', 
   task: 'Walk over here', 
   complete: false, }, ]; 
   const todoReducer = (state, action) => 
   { switch (action.type) 
    { 
     case 'DO_TODO': 
     return state.map(todo => { if (todo.id === action.id) 
      { return { ...todo, complete: true }; } 
      else { return todo; } }); 
      case 'UNDO_TODO':
       return state.map(todo => { if (todo.id === action.id) 
        { return { ...todo, complete: false }; } 
        else { return todo; } }); default: return state; } }; 
        const App = () => {
           const [todos, dispatch] = React.useReducer( todoReducer, initialTodo ); 
           const handleChange = todo => { dispatch({ type: todo.complete ? 'UNDO_TODO' : 'DO_TODO', id: todo.id, }); };
            return ( <ul> {todos.map(todo => (
               <li key={todo.id}> <label>
               <input type="checkbox" checked={todo.complete} onChange={() => handleChange(todo)} /> {todo.task}
                </label> 
                </li> ))} 
                </ul> ); 
                }; 
                export default App;*/
