import React, {useState} from 'react';
import {View,Button,Text } from 'react-native';

const Parent=({childern})=>{
    const [count,setCount]=useState(0);
    const clickBtnHandler=()=>
    {
        setCount(count=>count+1); 
    }
    return(
        <View>
            <Text>{count}</Text>
        <Button title='update'
                onPress={clickBtnHandler}/>
        {childern}</View>
    )
}
export default Parent;