import React from 'react';
import {StyleSheet} from 'react-native';
import {Appbar, Drawer} from 'react-native-paper';
import App from '../../../App';
import RootDrawerNavigator from './Main';
import Menus from './Menus';

const Appbars = ({navigation, previous}) => {
  const _search = () => console.log('Searching');
  return (
    <Appbar.Header>
     {!previous ? (
        <Appbar.Action
          icon="menu"
          onPress={() => {
            navigation.openDrawer();
          }}
        />
      ) : (
        <Appbar.BackAction onPress={navigation.goBack} />
      )}

      <Appbar.Content title="Practice Paper" subtitle="First App" />
      <Appbar.Action icon="magnify" onPress={_search} />
      {!previous ? <Menus /> : null}
    </Appbar.Header>
  );
};


/*const AppBottom = () => {
  return (
      <Appbar.Action
        icon="archive"
        onPress={() => console.log('Pressed archive')}
      />
      <Appbar.Action icon="mail" onPress={() => console.log('Pressed mail')} />
      <Appbar.Action
        icon="label"
        onPress={() => console.log('Pressed label')}
      />
      <Appbar.Action
        icon="delete"
        onPress={() => console.log('Pressed delete')}
      />
      <Appbar.Action icon="pen" onPress={() => console.log('pressed add')} />
  
  );
};*/

export default Appbars;
const styles = StyleSheet.create({
  bottom: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
  },
  colr: {
    backgroundColor: '#03DAC5',
  },
});
