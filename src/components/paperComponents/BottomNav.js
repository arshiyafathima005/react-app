import React,{useState} from 'react';
import {BottomNavigation,Text} from 'react-native-paper';
const HomeRoute = () => <Text></Text>;

const AlbumsRoute = () => <Text></Text>;

const RecentsRoute = () => <Text></Text>;
const FavoriteRoute = () => <Text></Text>;
const BottomNav=()=>{
    const[index,setIndex]=useState(0);
    const[routes]=useState([
        {key:'home',title:'Home',icon:'home',color: '#3F51B5'},
        {key:'albums',title:'Album',icon:'album',color: '#009688'},
        {key:'recents',title:'Recents',icon:'history',color: '#795548'},
        {key:'favorites',title:'Favorites',icon:'heart',badge:1,color: '#607D8B'}
    ]);
   const renderScene=BottomNavigation.SceneMap({
        home: HomeRoute,
        albums: AlbumsRoute,
        recents: RecentsRoute,
        favorites:FavoriteRoute
    });
    
    return(
        <BottomNavigation
        navigationState={{index,routes}}
        onIndexChange={setIndex}
        renderScene={renderScene}
        activeColor='#03DAC5'
        inactiveColor='#ffffffff'
        //sceneAnimationEnabled={false}
        //shifting={false} 
       // keyboardHidesNavigationBar
                    
       >
         </BottomNavigation>
    );
}

export default BottomNav;