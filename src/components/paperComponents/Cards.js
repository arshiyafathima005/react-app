import React from 'react';
import {StyleSheet, ScrollView} from 'react-native';
import {Card, Paragraph, Avatar, IconButton,Button, Divider} from 'react-native-paper';


const Cards=()=>{
return(
   <Card style={styles.card}>
    <Card.Title 
        title="Suhana"
        subtitle="Nature Lover"
        left={(props)=><Avatar.Image {...props} source={{uri: 'https://i.pinimg.com/originals/6d/ea/46/6dea46a6b54ff8dfbdc56e5b22064379.jpg'}}/>}
        right={(props)=><IconButton {...props} icon="more" onPress={() => {}}/>}
    />
    <Divider inset={true}/>
    <Card.Content>
         <Paragraph>Traditionally, a text is understood to be a piece of written or spoken material in its primary form (as opposed to a paraphrase or summary). A text is any stretch of language that can be understood in context. </Paragraph>
    </Card.Content>
    <Card.Cover source={{ uri: 'https://i.ytimg.com/vi/6lt2JfJdGSY/maxresdefault.jpg' }} />
     <Card.Actions>
        <Button icon="thumb-up-outline" mode="text">Like</Button>
        <Button icon="thumb-down-outline" mode="text">Unlike</Button>
    </Card.Actions> 
</Card>

);
}
export default Cards;
const styles=StyleSheet.create({
    card:{
    width:340,
    height:450,
    borderRadius:30,
    margin: 10,
    borderWidth:2,
        borderColor:'#ddd',
        borderBottomWidth:0,
        elevation:7,
        
    },
   
    });
