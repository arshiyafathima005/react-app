import React, {useState} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {
  createDrawerNavigator,
  DrawerItem,
  DrawerContentScrollView,
} from '@react-navigation/drawer';
import {
  Avatar,
  Drawer,
  Title,
  Caption,
  Paragraph,
  Switch,
  Divider,
  List,
} from 'react-native-paper';
import Forms from './Forms'
import FirstScreen from '../FirstPage';
const FirstDrawer = createDrawerNavigator();

function DrawerContent(props) {
  const [active, setActive] = React.useState('');

  return (
    <DrawerContentScrollView {...props}>
      <View style={styles.drawerContent}>
        <View style={styles.userInfo}>
          <View style={styles.image}>
            <Avatar.Image
              source={{
                uri:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRl6mkjvdm6YMIY2f19LgfCGOjVFyB9q0m6Dw&usqp=CAU',
              }}
              size={60}
            />

            <Title style={styles.title}>Alexa Alexander</Title>
            <Caption style={styles.caption}>@alexalexander</Caption>
            <View style={styles.row}>
              <View style={styles.innerRow}>
                <Paragraph style={styles.paragraph}>101</Paragraph>
                <Caption style={styles.caption}>Following</Caption>
              </View>
              <View style={styles.innerRow}>
                <Paragraph style={styles.paragraph}>99</Paragraph>
                <Caption style={styles.caption}>Followers</Caption>
              </View>
            </View>
          </View>
          <Divider />
          <Drawer.Section title="Screens">
            <Drawer.Item
              label="Home"
              active={active === 'first'}
              onPress={() => setActive('first')}
              icon="home"
            />
            <Drawer.Item
              label="Details"
              active={active === 'second'}
              onPress={() => setActive('second')}
              icon="account-details"
            />
            <Drawer.Item
              label="Email"
              active={active === 'third'}
              onPress={() => setActive('third')}
              icon="email"
            />
          </Drawer.Section>
          <Drawer.Section title="Prefernces">
            <View sytle={styles.switchText}>
              <Switchs />
            </View>
          </Drawer.Section>
          <Drawer.Section title="More">
            <Lists />
          </Drawer.Section>
        </View>
      </View>
    </DrawerContentScrollView>
  );
}
/*function FirstScreen() {
  return (
    <Forms/>
  );
}*/
const RootDrawerNavigator = () => {
  return (
    <FirstDrawer.Navigator drawerContent={() => <DrawerContent />}>
      <FirstDrawer.Screen name="FirstScreen" component={FirstScreen} />
    </FirstDrawer.Navigator>
  );
};
const Switchs = () => {
  const [switchOn, setSwitchOn] = useState(false);
  return (
    <View style={{flexDirection: 'row'}}>
      <Text style={styles.switchText}>Dark Theme</Text>
      <Switch
        value={switchOn}
        onValueChange={() => setSwitchOn(!switchOn)}
        //disabled={true}
        color="#9013fe"
      />
    </View>
  );
};
const Lists = () => {
  const [expanded, setExpanded] = React.useState(true);

  const handlePress = () => setExpanded(!expanded);

  return (
    <List.Section>
      <List.Accordion
        title="Settings"
        left={props => <List.Icon {...props} icon="cog" />}>
        <List.Item title="Wi-Fi" />
        <List.Item title="Mobile Network" />
      </List.Accordion>

      <List.Accordion
        title="Help"
        left={props => <List.Icon {...props} icon="help-circle" />}
        expanded={expanded}
        onPress={handlePress}>
        <List.Item title="Contact Us" />
        <List.Item title="Dictionary" />
        <List.Item title="Tips" />
      </List.Accordion>
    </List.Section>
  );
};

export default RootDrawerNavigator;
const styles = StyleSheet.create({
  drawerContent: {
    flex: 1
  },
  userInfo: {
    paddingLeft: 20,
    paddingTop: 10,
  },
  title: {
    marginTop: 20,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 13,
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  innerRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
  },
  switchText: {
    marginRight: 100,
    marginLeft: 20,
  },
  image:{
    flex:1,
    alignItems:"center",
    justifyContent:"center",
    
  }
});
