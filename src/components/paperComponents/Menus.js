import React,{useState} from 'react';
import {View} from 'react-native';
import {Menu,Provider,Appbar,Divider} from 'react-native-paper';
const Menus = () => {
    const [visible, setVisible] = useState(false);
    const openMenu = () => setVisible(true);
    const closeMenu = () => setVisible(false);
    return (
      <Provider>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
          }}>
          <Menu
            visible={visible}
            onDismiss={closeMenu}
            anchor={
              <Appbar.Action
                icon="dots-vertical"
                color="#ffffff"
                onPress={openMenu}
              />
            }>
            <Menu.Item
              title="Home"
              icon="home"
              onPress={() => {
                console.log('Home was pressed');
              }}
            />
            <Menu.Item
              title="Details"
              icon="details"
              onPress={() => {
                console.log('Details 1 was pressed');
              }}
            />
            <Menu.Item title="Upload" icon="upload" onPress={() => {}} />
            <Menu.Item
              title="Save"
              icon="cloud"
              onPress={() => {
                console.log('Add 1 was pressed');
              }}
            />
            <Divider />
            <Menu.Item
              title="Delete"
              icon="delete"
              disabled="true"
              onPress={() => {}}
            />
          </Menu>
        </View>
      </Provider>
    );
  };
  export default Menus;