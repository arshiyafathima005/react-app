import React, {useState} from 'react';
import {StyleSheet, View, ImageBackground, ToastAndroid} from 'react-native';
import {
  Modal,
  Portal,
  Title,
  Button,
  Provider,
  Divider,
  Text,
  Headline,
  TextInput,
} from 'react-native-paper';
//import DatePicker from 'react-native-date-picker';

const Modals = () => {
  const [visible, setVisible] = useState(false);
  //const [date, setDate] = useState(new Date());
  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);
  const containerStyle = {backgroundColor: 'white', margin: 20};

  const modalHeader = (
    <View style={styles.modalHeader}>
      <ImageBackground
        style={styles.image}
        source={{
          uri:
            'https://png.pngtree.com/thumb_back/fh260/back_our/20190628/ourmid/pngtree-blue-background-with-geometric-forms-image_280879.jpg',
        }}>
        <Title style={styles.title}>SUBMIT APPLICATION</Title>
      </ImageBackground>
      <Divider />
    </View>
  );
  const modalBody = (
    <View>
      <Headline style={{fontWeight: 'bold', fontSize: 20, textAlign: 'center'}}>
        ENTER OTP
      </Headline>
      <Text style={{textAlign: 'center'}}>Please check your phone...!</Text>
      <TextInput
        label="OTP"
        keyboardType="numeric"
        mode="outlined"
        maxLength={6}
        style={styles.textInptStyles}
        right={<TextInput.Icon color="#8C8C8C" icon="eye" size={30} />}
        style={styles.textInput}
      />
      {/*  <DatePicker
        date={date}
        onDateChange={setDate}
        style={{marginRight: 10}}
        mode={'date'}
        androidVariant="nativeAndroid"
        //locale="en"
      /> */}
    </View>
  );
  const modalFooter = (
    <View style={styles.modalFooter}>
      <Divider style={styles.divider} />
      <View style={{flexDirection: 'row', justifyContent: 'center'}}>
        <Button mode="contained" onPress={hideModal} style={styles.btn}>
          DISMISS
        </Button>
        <View style={styles.verticalLine} />
        <Button
          mode="contained"
          onPress={() =>
            ToastAndroid.show('SUBMITION SUCESSFUL!', ToastAndroid.SHORT)
          }
          style={styles.btn}>
          SUBMIT
        </Button>
      </View>
    </View>
  );

  return (
    <Provider>
      <Portal>
        <Modal
          visible={visible}
          onDismiss={hideModal}
          dismissable={false}
          contentContainerStyle={containerStyle}
          style={styles.modal}>
          {modalHeader}
          {modalBody}
          {modalFooter}
        </Modal>
      </Portal>
      <Button style={{marginTop: 30}} onPress={showModal}>
        Show Date
      </Button>
    </Provider>
  );
};
export default Modals;
const styles = StyleSheet.create({
  title: {
    color: '#ffffff',
    margin: 30,
    padding: 10,
    fontWeight: 'bold',
  },
  modalHeader: {height: 70},
  image: {height: 60},
  modalFooter: {
    height: 50,
  },
  divider: {
    backgroundColor: '#909090',
  },
  verticalLine: {
    height: '100%',
    width: 0.4,
    backgroundColor: '#909090',
    margin: 0,
  },
  textInput: {
    margin: 20,
  },
  btn: {
    margin: 6,
    paddingHorizontal: 25,
  },
});
