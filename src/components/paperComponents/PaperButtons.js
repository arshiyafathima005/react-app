import React from 'react';
import {Button,ActivityIndicator,Colors,configureFonts} from 'react-native-paper';
const ActivityIndicatorBtn=()=><ActivityIndicator animating={true} color={Colors.white} />

const PaperButton=()=>
{
return(
//<Button icon={()=>(<ActivityIndicator animating={true} color={Colors.white} />)} mode="contained" onPress={()=>console.log('Pressed Loading')}>
// <Button icon={ActivityIndicatorBtn} mode="contained" onPress={()=>console.log('Pressed Loading')}>
    <Button style={{margin:6}} icon="loading" mode="contained" onPress={()=>console.log('Pressed Loading')} loading={true} color="#1a61d8" contentStyle={{ height: 40,flexDirection: 'row-reverse' }}>
  
      Load More
    </Button>
    )
}
export default PaperButton;