import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import FirstScreen from '../FirstPage';
import DetailsScreen from '../SecondPage';

export const FeedStack = () => {
  return (
    <Stack.Navigator initialRouteName="Feed">
      <Stack.Screen
        name="Home"
        component={FirstScreen}
       // options={{ headerTitle: 'Twitter' }}
      />
      <Stack.Screen
        name="Details"
        component={DetailsScreen}
       // options={{ headerTitle: 'Tweet' }}
      />
    </Stack.Navigator>
  );
};