import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Appbar} from 'react-native-paper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const AppBars = ({navigation, previous, scene}) => {
  const {options} = scene.descriptor;
  const title =
    options.headerTitle !== undefined ? options.headerTitle : scene.route.name;
  return (
    <View style={styles.appBars}>
      <Appbar.Header style={{backgroundColor: '#123D7A'}}>
        {!previous ? null : <Appbar.BackAction onPress={navigation.goBack} />}
        <Appbar.Content title={title} />
        <Appbar.Action icon="cog" onPress={() => {navigation.openDrawer()}} />
      </Appbar.Header>
    </View>
  );
};
export default AppBars;
const styles = StyleSheet.create({
  appBars: {
    height: hp('5%'),
    width: wp('100%'),
    backgroundColor: '#123D7A',
  },
});
