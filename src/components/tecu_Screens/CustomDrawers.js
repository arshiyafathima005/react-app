import React from 'react';
import {View, StyleSheet} from 'react-native';
import {DrawerItem, DrawerContentScrollView} from '@react-navigation/drawer';
import {Drawer, Divider, Text} from 'react-native-paper';
const CustomDrawers = () => {
  return (
    <DrawerContentScrollView>
      <View style={styles.drawerContent}>
        <View style={{flex: 2, marginTop: '50%'}}>
          <Drawer.Section>
            <Divider />
            <DrawerItem
              label={() => <Text style={styles.drawer_item_text}>Privacy</Text>}
              onPress={() => {}}
            />
            <Divider />
            <DrawerItem
              label={() => <Text style={styles.drawer_item_text}>Terms</Text>}
              onPress={() => {}}
              focused={false}
            />
            <Divider />
            <DrawerItem
              label={() => <Text style={styles.drawer_item_text}>FAQ</Text>}
              onPress={() => {}}
              //focused={true}
            />
            <Divider />
            <DrawerItem
              label={() => (
                <Text style={styles.drawer_item_text}>Contact Us</Text>
              )}
              onPress={() => {}}
            />
          </Drawer.Section>
        </View>

        <View style={{flex: 1, marginTop: '130%', marginHorizontal: '6%'}}>
          <Text style={styles.text}>
            TECU Credit Union Co-Operative Society
          </Text>
          <Text style={styles.text}>Copyright 2015-2020</Text>
        </View>
      </View>
    </DrawerContentScrollView>
  );
};
export default CustomDrawers;
const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  text: {
    fontWeight: '700',
    fontSize: 10,
  },
  drawer_item_text: {
    color: '#123D7A',
    fontWeight: '700',
    fontSize: 17,
  },
});
