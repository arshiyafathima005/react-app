import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Button} from 'react-native-paper';
import {useNavigation} from '@react-navigation/native';
const DualButton = ({screenPrevoius,screenNext}) => {
  const navigation = useNavigation();
  return (
    <View style={{flexDirection: 'row', justifyContent:'space-between',marginHorizontal:'4%'}}>
      <Button
        style={styles.btn}
        mode="contained"
        onPress={() => navigation.navigate(screenPrevoius)}
        color="#E57A1B"
        contentStyle={{height: 50}}
        dark={true}>
        Previous
      </Button>
      <Button
        style={styles.btn}
        mode="contained"
        onPress={() => navigation.navigate(screenNext)}
        color="#E57A1B"
        contentStyle={{height: 50}}
        dark={true}>
        Next
      </Button>
    </View>
  );
};
export default DualButton;
const styles = StyleSheet.create({
  btn: {
    flex: 1,
    marginBottom: '2%',
    fontWeight: 'bold',
    margin:'1%'
  },
});
