import React from 'react';
import {StyleSheet, View, Card} from 'react-native';
import {Text, Divider} from 'react-native-paper';
import ImageMatchCard from './ImageMatchCard';
import DualButton from './DualButton';
import PreviewDocuments from './PreviewDocuments';
import ScanPdf from './ScanPdf';

const FaceMatch = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={{flex: 1.2}}>
        <Text style={styles.text}>Please Wait getting face match results</Text>
        <Text style={styles.primary_capture_text}>
          Primary Document Face V/S Capture Face
        </Text>
        <ImageMatchCard />
        <Text style={styles.result_text}>Face Match Percentage %</Text>
      </View>
      <Divider />
      <View style={{flex: 1}}>
        <Text style={styles.primary_capture_text}>
          Secondary Document Face V/S Capture Face
        </Text>
        <ImageMatchCard />
        <Text style={styles.result_text}>Face Match Percentage %</Text>
      </View>
      <View style={{flex: 0.2}}>
        <DualButton screenPrevoius={PreviewDocuments} screenNext={ScanPdf} />
      </View>
    </View>
  );
};
export default FaceMatch;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  text: {
    fontWeight: 'bold',
    fontFamily: 'arial',
    fontSize: 16,
    textAlign: 'center',
    marginTop: '10%',
    marginHorizontal: '6%',
  },
  primary_capture_text: {
    fontFamily: 'arial',
    fontSize: 14,
    textAlign: 'left',
    marginHorizontal: '6%',
    marginTop: '4%',
    fontWeight: 'bold',
  },
  result_text: {
    fontFamily: 'arial',
    fontSize: 14,
    textAlign: 'center',
    marginHorizontal: '6%',
    marginTop:'-6%',
    fontWeight: 'bold',
  },
});
