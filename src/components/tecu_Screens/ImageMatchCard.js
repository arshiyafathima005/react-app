import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {Card} from 'react-native-elements';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const ImageMatchCard = () => {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: '10%',
      }}>
      <Card containerStyle={styles.card} wrapperStyle={styles.inner_card_style}>
        <View>
          <Card.Image
            containerStyle={styles.userImage}
            source={require('../../assets/user-image.png')}
          />
          {/*   <View style={{marginVertical: '12%'}}>
            <Text style={styles.text}>V/S</Text>
          </View>

          <Card.Image
            containerStyle={styles.userImage}
            source={require('../../assets/user-image.png')}
          /> */}
        </View>
      </Card>

      <Text style={styles.text}>V/S</Text>
      <Card containerStyle={styles.card} wrapperStyle={styles.inner_card_style}>
        <View>
          <Card.Image
            containerStyle={styles.userImage}
            source={require('../../assets/user-image.png')}
          />
          {/*   <View style={{marginVertical: '12%'}}>
            <Text style={styles.text}>V/S</Text>
          </View>

          <Card.Image
            containerStyle={styles.userImage}
            source={require('../../assets/user-image.png')}
          /> */}
        </View>
      </Card>
    </View>
  );
};
export default ImageMatchCard;
const styles = StyleSheet.create({
  card: {
    height: hp('20%'),
    width: wp('35%'),
    resizeMode: 'center',
    alignSelf: 'center',
    elevation: 10,
    borderRadius: 6,
  },
  userImage: {
    height: hp('15%'),
    width: wp('25%'),
    resizeMode: 'contain',
    alignSelf: 'auto',
    paddingBottom: '1%',
  },
  text: {
    fontWeight: 'bold',
    fontFamily: 'arial',
    fontSize: 20,
    marginTop: '20%',
  },
});
