import React from 'react';
import {StyleSheet, View, TextInput} from 'react-native';
import {Card} from 'react-native-elements';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Text, IconButton,} from 'react-native-paper';
import ReusableButton from './ReusableButton';
const PersonalInformation = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={{flex: 2.5}}>
        <Card containerStyle={styles.card} wrapperStyle={{margin: '10%'}}>
          <Card.Image
            wrapperStyle={styles.userImage}
            source={require('../../assets/user-image.png')}
          />
          <Text style={styles.text_take_pic}>Take Picture</Text>
          <IconButton
            icon="camera-outline"
            size={20}
            onPress={() => {
              navigation.navigate('ScanDocuments');
            }}
            style={styles.camera_btn}
          />
        </Card>
      </View>
      <View style={{flex: 2}}>
        <Text style={styles.location_title}>Location of picture</Text>
        <Card containerStyle={styles.card_edit_txt}>
       
          <TextInput style={styles.location_text}>420, SH 35, INNER VALLEY, WHITEFIELD, BENGALURU, KARNATAKA 560066,
         INDIA</TextInput> 
        {/*    //value={item.value}
        label='420, SH 35, INNER VALLEY, WHITEFIELD, BENGALURU, KARNATAKA 560066,
          INDIA'
         // mode="flat"
          //onChangeText={text=>setText(text)}
          //theme={{colors: {primary: '#E57A1B'}}}
          //disabled={true}
          underlineColorAndroid="#ffffff"
         multiline={true}
          autoCapitalize="words"
          autoCompleteType="name"
          isFocused={false}
          /> */}
            
        
        </Card>
      </View>
    <ReusableButton screenName="ScanDocuments" />
    </View>
  );
};
export default PersonalInformation;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    height: '90%',
    width: '60%',
    resizeMode: 'center',
    margin: '8%',
    alignSelf: 'center',
    elevation: 10,
    borderRadius: 6,
  },
  userImage: {
    height: hp('50%'),
    width: wp('40%'),
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  text_take_pic: {
    fontWeight: 'bold',
    fontFamily: 'arial',
    fontSize: 18,
    textAlign: 'center',
  },
  camera_btn: {
    backgroundColor: '#ffffff',
    borderColor: '#cccccc',
    borderWidth: 2,
    alignSelf: 'center',
  },
  location_title: {
    fontWeight: 'bold',
    fontFamily: 'arial',
    fontSize: 16,
    textAlign: 'left',
    marginHorizontal: '6%',
    marginVertical: '3%',
  },
  card_edit_txt: {
    height: '40%',
    width: '90%',
    resizeMode: 'center',
    marginVertical: '0%',
    marginHorizontal: '6%',
    alignSelf: 'center',
    elevation: 4,
  },
  location_text: {
    fontWeight: '700',
    fontFamily: 'arial',
    fontSize: 15,
    textAlign: 'left',
  },
});
