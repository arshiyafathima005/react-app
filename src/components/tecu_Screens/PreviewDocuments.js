import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
} from 'react-native';
import {TextInput} from 'react-native-paper';
import {Card} from 'react-native-elements';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ReusableButton from './ReusableButton';
import {HideWithKeyboard} from 'react-native-hide-with-keyboard';
import Collapsible from 'react-native-collapsible';

const PreviewDocuments = ({navigation}) => {
  const [userData, fetchUserData] = useState([
    {key: '1', name: 'Status Code', value: ''},
    {key: '2', name: 'Document #', value: ''},
    {key: '3', name: 'Date of Expiry', value: ''},
    {key: '4', name: 'Date of Issue #', value: ''},
    {key: '5', name: 'Address', value: ''},
  ]);
  const [collapsed, setCollapsed] = useState(true);
  return (
    <View style={styles.container}>
      <View style={{flex: 1.5}}>
        <View style={{flex: 1.5}}>
          <Text style={styles.text}>
            Trinidad and Tobago - National ID Card
          </Text>
          <Card
            containerStyle={styles.card}
            wrapperStyle={styles.inner_card_style}>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                alignSelf: 'flex-start',
              }}>
              <Card.Image
                containerStyle={styles.userImage}
                source={require('../../assets/user-image.png')}
              />
              <View
                style={{
                  flexDirection: 'column',
                  flex: 2,
                  alignSelf: 'flex-end',
                  marginVertical: '-45%',
                  marginLeft: '2%',
                  padding: '10%',
                  resizeMode: 'center',
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <Text style={styles.fetched_text}>Name -</Text>
                  <Text style={styles.fetched_text}>Ivana </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <Text style={styles.fetched_text}>Age -</Text>
                  <Text style={styles.fetched_text}>28 Years </Text>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <Text style={styles.fetched_text}>Gender -</Text>
                  <Text style={styles.fetched_text}>Female </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <Text style={styles.fetched_text}>Signature </Text>
                  <View
                    style={{
                      flex: 1,
                      backgroundColor: '#cccccc',
                      paddingHorizontal: '32%',
                    }}></View>
                </View>
              </View>
            </View>
          </Card>
        </View>
       
        <View style={{flex: 1}}>
        
            <Text style={styles.passport_title}>Passport Copy</Text>
            <View
              style={{
                flex: 1,
                marginHorizontal: '6%',
                marginVertical: '1%',
                flexDirection: 'row',
                justifyContent: 'space-evenly',
              }}>
              <View
                style={{
                  flex: 1,
                  marginVertical: '2%',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity onPress={() => {}}>
                  <Image
                    style={styles.card_image}
                    source={require('../../assets/imageScanner.jpg')}></Image>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  flex: 1,
                  marginLeft: '6%',
                  marginVertical: '2%',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity onPress={() => {}}>
                  <Image
                    style={styles.card_image}
                    source={require('../../assets/imageScanner.jpg')}></Image>
                </TouchableOpacity>
              </View>
            </View>
           
          </View>
        
      </View>
      <View style={{flex: 1}}>
        <FlatList
          data={userData}
          renderItem={({item}) => (
            <View>
              <Text style={styles.data_field_text}>{item.name}</Text>
              <TouchableOpacity onPress={() => {collapse=>setCollapsed(collapsed)}}>
                <TextInput
                  //value={item.value}
                  mode="outlined"
                  //onChangeText={text=>setText(text)}
                  theme={{colors: {primary: '#E57A1B'}}}
                  style={styles.textInptStyles}
                  autoCapitalize="words"
                  autoCompleteType="name"
                />
              </TouchableOpacity>
            </View>
          )}
        />
        <HideWithKeyboard>
          <ReusableButton screenName="FaceMatch" />
        </HideWithKeyboard>
      </View>
    </View>
  );
};
export default PreviewDocuments;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  text: {
    fontWeight: 'bold',
    fontFamily: 'arial',
    fontSize: 16,
    textAlign: 'center',
    marginTop: '10%',
  },
  card: {
    height: '60%',
    width: '90%',
    resizeMode: 'center',
    alignSelf: 'center',
    elevation: 10,
    borderRadius: 6,
  },

  userImage: {
    height: hp('15%'),
    width: wp('25%'),
    resizeMode: 'contain',
    alignSelf: 'auto',
    paddingBottom: '1%',
  },
  sign_image: {
    height: hp('5%'),
    width: wp('25 %'),
    resizeMode: 'contain',
    alignSelf: 'auto',
  },
  fetched_text: {
    fontWeight: 'bold',
    fontFamily: 'arial',
    fontSize: 16,
    textAlign: 'left',
  },
  passport_title: {
    fontWeight: 'bold',
    fontFamily: 'arial',
    fontSize: 16,
    textAlign: 'left',
    marginLeft: '6%',
  },
  data_field_text: {
    fontWeight: 'bold',
    fontFamily: 'arial',
    fontSize: 12,
    textAlign: 'left',
    marginLeft: '6%',
  },
  textInptStyles: {
    marginHorizontal: '6%',
    fontSize: 16,
  },
  view_doc_text: {
    fontWeight: '700',
    fontFamily: 'arial-Medium',
    fontSize: 18,
    textAlign: 'left',
    alignItems: 'stretch',
    marginVertical: '6%',
    marginHorizontal: '6%',
  },
  card_image: {
    alignItems: 'flex-end',
    height: hp('30%'),
    width: wp('35%'),
    resizeMode: 'contain',
    marginVertical: '1%',
    marginHorizontal: '6%',
  },
});
