import React from 'react';
import {StyleSheet} from 'react-native';
import {Button} from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
const buttonPressed = ({propsyran}) => {
    
  };
const ReusableButton = ( { screenName}) => {
  
  const navigation = useNavigation();
return (
    <Button
      style={styles.btn}
      mode="contained"
      onPress={() => navigation.navigate(screenName)}
      color="#E57A1B"
      contentStyle={{height: 50}}
      dark={true}>
      Next
    </Button>
  );
};
export default ReusableButton;
const styles=StyleSheet.create(
  {
    btn:{
     marginHorizontal:'6%',
   
     marginBottom:'4%',
     fontWeight:'bold'
    }
  }
)