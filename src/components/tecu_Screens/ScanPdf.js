import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Card} from 'react-native-elements';
import {Text} from 'react-native-paper';
import DualButton from './DualButton';
import ReusableButton from './ReusableButton';
 
const ScanPdf = ({navigation}) => {
  const [state, setState] = useState(true);
  const [pdf, fetchPdfData] = useState([
    {
      key: '1',
      name: 'Member onboarding',
      src: require('../../assets/scanPdf.png'),
      doc: require('../../assets/doc.png'),
      pdf: require('../../assets/pdf.png'),
    },
    {
      key: '2',
      name: 'LINCU',
      src: require('../../assets/scanPdf.png'),
      doc: require('../../assets/doc.png'),
      pdf: require('../../assets/pdf.png'),
    },
    {
      key: '3',
      name: 'Cuna',
      src: require('../../assets/scanPdf.png'),
      doc: require('../../assets/doc.png'),
      pdf: require('../../assets/pdf.png'),
    },
  ]);
 
  return (
    <View style={styles.container}>
      {state ? (
        <View>
          <Text style={styles.text}>Scan Any Documents below</Text>
          <FlatList
            data={pdf}
            renderItem={({item}) => (
              <View>
                <Card
                  containerStyle={{borderRadius: 6, elevation: 6}}
                  wrapperStyle={{}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text style={styles.pdf_title}>{item.name}</Text>
                    <TouchableOpacity onPress={() => {}}>
                      <Image
                        style={styles.card_image}
                        source={item.src}></Image>
                    </TouchableOpacity>
                  </View>
                </Card>
              </View>
            )}
          />
          <View style={{marginTop:'40%'}}>
            <DualButton screenPrevoius="PreviewDocuments" screenNext='Preview'/>
          </View>
        </View>
      ) : (
        <View>
          <Text style={styles.text}>View the below Documents</Text>
          <FlatList
            data={pdf}
            renderItem={({item}) => (
              <View>
                <Card containerStyle={{borderRadius: 6}} wrapperStyle={{}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-evenly',
                      }}>
                      <TouchableOpacity onPress={() => {}}>
                        <Image
                          style={styles.view_doc_pdf}
                          source={item.pdf}></Image>
                      </TouchableOpacity>
                      <Text style={styles.view_doc_text}>{item.name}</Text>
                    </View>
                    <TouchableOpacity onPress={() => {}}>
                      <Image
                        style={styles.card_image}
                        source={item.doc}></Image>
                    </TouchableOpacity>
                  </View>
                </Card>
              </View>
            )}
          />
          <View style={{marginTop:'40%'}}>
            <DualButton screenPrevoius="PreviewDocuments" screenNext='Preview'/>
          </View>
        </View>
      )}
    </View>
  );
};
export default ScanPdf;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text: {
    fontWeight: 'bold',
    fontFamily: 'arial',
    fontSize: 16,
    textAlign: 'left',
    marginTop: '10%',
    marginHorizontal: '6%',
  },
  card: {
    height: '100%',
    width: '90%',
    resizeMode: 'center',
    alignSelf: 'center',
    elevation: 10,
    borderRadius: 6,
  },
  pdf_title: {
    fontWeight: '700',
    fontFamily: 'arial-Medium',
    fontSize: 18,
    textAlign: 'left',
    alignItems: 'flex-start',
    marginVertical: '6%',
    marginHorizontal: '6%',
  },
  card_image: {
    alignItems: 'flex-end',
    height: hp('10%'),
    width: wp('10%'),
    resizeMode: 'contain',
    marginVertical: '1%',
  },
  view_doc_pdf: {
    alignItems: 'flex-start',
    height: hp('10%'),
    width: wp('10%'),
    resizeMode: 'contain',
  },
  view_doc_text: {
    fontWeight: '700',
    fontFamily: 'arial-Medium',
    fontSize: 18,
    textAlign: 'left',
    alignItems: 'stretch',
    marginVertical: '6%',
    marginHorizontal: '6%',
  },
});
