import React from 'react';
import {StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import {Text} from 'react-native-paper';
import ReusableButton from './ReusableButton';
const ScanPrimaryDocuments = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={{flex: 5, backgroundColor: '#E57A1B'}}>
        <Image
          source={require('../../assets/scan-doc.png')}
          style={styles.scan_doc_image}
        />
        <Text style={styles.text}>You Need To Upload Your</Text>
        <Text style={styles.nationalId_title}>National ID</Text>
        <Text style={styles.id_text}>Ending With XXXXXXX3456</Text>
      </View>
      <View style={{flex: 2}}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('PreviewDocuments');
          }}
          style={styles.touchable_btn}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              alignSelf: 'center',
            }}>
            <Image
              source={require('../../assets/camera-btn.png')}
              style={styles.camera_btn}
            />
            <Text style={styles.camera_btn_txt}>Use Camera</Text>
          </View>
        </TouchableOpacity>
      </View>
      <ReusableButton screenName='PreviewDocuments' />
    </View>
  );
};
export default ScanPrimaryDocuments;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scan_doc_image: {
    resizeMode: 'center',
    height: '40%',
    width: '40%',
    alignSelf: 'center',
    marginTop: '20%',
  },
  text: {
    fontWeight: 'normal',
    fontFamily: 'arial',
    fontSize: 20,
    textAlign: 'left',
    paddingTop: '3%',
    alignSelf: 'center',
    color: '#ffffff',
  },
  nationalId_title: {
    fontWeight: 'bold',
    fontFamily: 'arial',
    fontSize: 26,
    textAlign: 'left',
    alignSelf: 'center',
    color: '#ffffff',
  },
  id_text: {
    fontWeight: 'normal',
    fontFamily: 'arial',
    fontSize: 20,
    textAlign: 'left',
    alignSelf: 'center',
    color: '#ffffff',
  },
  touchable_btn: {
    backgroundColor: '#ffffff',
    alignSelf: 'center',
    marginVertical: '-5%',
    borderRadius: 6,
    marginHorizontal: '20%',
    padding:'1%',
    elevation: 2,
  },
  camera_btn: {
    resizeMode: 'contain',
    height: '50%',
    width: '50%',
    marginTop: '4%',
    paddingLeft:'1%'
  },
  camera_btn_txt: {
    fontWeight: 'bold',
    fontFamily: 'arial',
    fontSize: 25,
    textAlign: 'left',
    paddingVertical: '2%',
    paddingRight: '20%',

   
  },
});
