import React from 'react';
import {StyleSheet, View, ScrollView, Image,TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AppBars  from './AppBars'
//const {width, height} = Dimensions.get('window');
import {
  Provider as PaperProvider,
  BottomNavigation,
  Card,
  Text,
  Button,
  Appbar,
  Title,
  Menu,
  Provider,
  Divider,
} from 'react-native-paper';

const SelfVerification = ({navigation}) => {
  return (
    <PaperProvider style={{backgroundColor: '#ffffff'}}>
      <View style={styles.container}>
        <View style={styles.insideContainer}>
          <Text style={styles.Verify_text}>
            Verify Yourself With All Your Documents And Profile Picture...
          </Text>
          <Card style={styles.card}>
            <View style={styles.image_mask_verifi_view}>
              <Image
                source={require('../../assets/mask-group-verification.png')}
                style={styles.image_mask_verification}
              />
            </View>
          </Card>
          <Card style={styles.card_press}>
            <Card.Content>
            <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('Details')
                  }}
                > 
                <Text style={styles.card_press_text}>Please Click on the Link Received via SMS / Email</Text>
            </TouchableOpacity>
             </Card.Content>
          </Card>
        </View>
      </View>
    </PaperProvider>
  );
};
export default SelfVerification;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  
  insideContainer: {
    height: hp('90%'),
    width: wp('100%'),
    alignSelf: 'center',
    marginTop: '10%',
    marginHorizontal: '25%',
  },
  Verify_text: {
    fontWeight: 'bold',
    fontFamily: 'arial',
    fontSize: 18,
    textAlign: 'left',
    marginHorizontal: '5%',
  },
  card: {
    resizeMode: 'center',
    padding: '20%',
    height: hp('35%'),
    width: wp('90%'),
    marginTop: '5%',
    marginHorizontal: '5%',
  },
  image_mask_verifi_view: {
    height: hp('35%'),
    width: wp('70%'),
    alignSelf: 'center',
    marginVertical: '15%',
  },
  image_mask_verification: {
    resizeMode: 'cover',
    height: '100%',
    width: '100%',
    alignSelf: 'center',
    marginTop: '-40%',
  },
  card_press: {
    resizeMode: 'center',
    height: hp('12%'),
    width: wp('80%'),
    alignSelf: 'center',
    elevation: 6,
  },
  card_press_text: {
    fontWeight: 'bold',
    fontFamily: 'arial',
    fontSize: 18,
    textAlign: 'center',
    margin: '2%',
  },
});
