import React from 'react';
import {View, Image, StyleSheet, Text, ToastAndroid} from 'react-native';

const SplashScreen = () => {
  React.useEffect(() => {
    ToastAndroid.show('Loaded!', ToastAndroid.SHORT);
  }, []);
  return (
    <View style={styles.container}>
      <Image
        source={require('../../assets/blob-1.png')}
        style={styles.blob_one}
      />
      <Image
        source={require('../../assets/Tecu_Logo/drawable-xxxhdpi/tecu-logo.png')}
        style={styles.tecu_logo}
      />
      <Image
        source={require('../../assets/blob-2.png')}
        style={styles.blob_two}
      />
      <Image
        source={require('../../assets/mask-group.png')}
        style={styles.mask_group}
      />
      <View style={{height: '8%', marginTop: '-45%'}}>
        <Text style={styles.smart_text}>Smartphone Verification App</Text>
        <Text style={styles.powered_text}>Powered By</Text>
      </View>
      <Image
        source={require('../../assets/Impacto.png')}
        style={styles.impacto_image}
      />
      <View style={{height: '8%', marginTop: '-3%'}}>
        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <Text style={styles.disclimer_text}>Disclaimer : </Text>
          <Text style={styles.disclimer_desc_text}>
            Strictly Not For Production Use
          </Text>
        </View>
      </View>
      <Image
        source={require('../../assets/blob-3.png')}
        style={styles.blob_three}
      />
    </View>
  );
};
export default SplashScreen;
const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
    backgroundColor: '#ffffff',
  },

  tecu_logo: {
    resizeMode: 'center',
    height: '50%',
    width: '50%',
    marginHorizontal: '25%',
    marginTop: '-40%',
  },
  blob_one: {
    height: '30%',
    width: '70%',
    marginTop: '-20%',
    marginLeft: '-20%',
  },
  blob_two: {
    width: '100%',
    height: '70%',
    marginHorizontal: '80%',
    marginTop: '-100%',
  },
  mask_group: {
    resizeMode: 'center',
    width: '80%',
    height: '90%',
    marginHorizontal: '10%',
    marginTop: '-102%',
  },
  smart_text: {
    fontWeight: 'bold',
    fontSize: 22,
    textAlign: 'center',
    fontFamily: 'arial',
  },
  powered_text: {
    fontWeight: 'bold',
    fontSize: 8,
    textAlign: 'center',
    fontFamily: 'arial',
    paddingTop: 16,
  },
  impacto_image: {
    height: '10%',
    width: '40%',
    alignSelf: 'center',
    marginHorizontal: '-50%',
  },
  disclimer_text: {
    fontWeight: 'bold',
    fontSize: 10,
    fontFamily: 'arial',
  },
  disclimer_desc_text: {
    fontSize: 10,
    fontFamily: 'arial',
  },
  blob_three: {
    width: '135%',
    height: '61%',
    marginLeft: '-30%',
    marginTop: '-15%',
  },
});
